#![no_std]
#![no_main]
// #![feature(const_loop)]
// #![feature(const_if_match)]
// extern crate panic_halt; /* you can put a breakpoint on `rust_begin_unwind` to catch panics */
extern crate panic_semihosting; /* logs messages to the host stderr; requires a debugger */

#[macro_use]
extern crate arrayref;

use stm32f1xx_hal::{gpio::*, pac, prelude::*, pwm, stm32, timer::*};

use embedded_hal::digital::v2::{InputPin, OutputPin};
use heapless::{
    consts::{U4, U8},
    HistoryBuffer, Vec,
};

mod tones;
use tones::*;

mod midi;
use midi::*;

const CARRIER_FREQ: u32 = 2 * 35156; // 72MHz / 2048
const SOUND_UPDATE_FREQ: u32 = 32768;
const MAIN_FREQ: u32 = 100;

pub struct LedRing {
    leds: [Pxx<Output<PushPull>>; 8],
}
impl LedRing {
    pub fn from(led_array: [Pxx<Output<PushPull>>; 8]) -> Self {
        LedRing { leds: led_array }
    }

    pub fn light_binary(&mut self, count: u8) {
        for i in 0..8 {
            if count & (1 << i) != 0 {
                self.leds[i].set_high().ok();
            } else {
                self.leds[i].set_low().ok();
            }
        }
    }

    pub fn light1(&mut self, count: u32) {
        let slowcount = count / 200;
        match slowcount % 72 {
            0..=24 => self.light_binary(1u8.rotate_right(slowcount as u32)),
            25..=48 => self.light_binary(0b00010001u8.rotate_right(slowcount as u32)),
            49..=72 => self.light_binary(0b01001001u8.rotate_right(slowcount as u32)),
            _ => self.light_binary(0b10101010u8.rotate_right(slowcount as u32)),
        }
    }
    pub fn light2(&mut self, count: u32) {
        let slowcount = count / 200;
        match (count / 20) % 1890 {
            // Hoch soll er leben
            0..=105 => self.light_binary(0b10001000u8.rotate_right(slowcount as u32)),
            // Hoch soll er leben
            106..=210 => self.light_binary(0b10001000u8.rotate_left(slowcount as u32)),
            // 3 x hoch
            211..=262 => self.light_binary(0b10101010u8.rotate_right(count / 30 as u32)), // schnellblink
            263..=315 => self.light_binary(0b10101010u8.rotate_right(count / 90 as u32)), // schnellblink
            316..=420 => self.light_binary(0b10101010u8.rotate_right(count / 50 as u32)), // schnellblink

            // Hoch soll er leben
            421..=525 => self.light_binary(0b10011001u8.rotate_right(slowcount as u32)),
            // Hoch soll er leben
            526..=630 => self.light_binary(0b10011001u8.rotate_left(slowcount as u32)),
            // 3 x hoch
            631..=682 => self.light_binary(0b10101010u8.rotate_right(count / 20 as u32)), // schnellblink
            683..=735 => self.light_binary(0b10101010u8.rotate_right(count / 80 as u32)), // schnellblink
            736..=840 => self.light_binary(0b10101010u8.rotate_right(count / 50 as u32)), // schnellblink

            // er lebe drei mal
            841..=990 => self.light_binary(0b10101010u8.rotate_right(slowcount as u32)),
            // hoch hoch hoch
            991..=1050 => self.light_binary(0b10101010u8.rotate_right(count / 40 as u32)), // schnellblink
            // er lebe drei mal
            1051..=1200 => self.light_binary(0b10101010u8.rotate_left(slowcount as u32)), // schnellblink
            // hoch hoch hoch
            1201..=1260 => self.light_binary(0b10101010u8.rotate_left(count / 20 as u32)), // schnellblink

            // erlebe drei mal
            1261..=1312 => self.light_binary(0b10001000u8.rotate_right(slowcount as u32)),
            // hoch hoch hoch
            1313..=1365 => self.light_binary(0b10101010u8.rotate_right(count / 20 as u32)), // schnellblink
            // erlebe drei mal
            1366..=1417 => self.light_binary(0b10001000u8.rotate_left(slowcount as u32)),
            // hoch hoch hoch
            1418..=1470 => self.light_binary(0b10101010u8.rotate_right(count / 20 as u32)), // schnellblink

            1471..=1732 => self.light_binary(0b00010001u8.rotate_right(slowcount as u32)), // schnellblink


            // HOOOCH
            1733..=1765 => self.light_binary(0b00001111u8),
            1766..=1818 => self.light_binary(0b11110000u8),
            1819..=1890 => self.light_binary(0b11111111u8),
            // 1891..=1732 => self.light_binary(0b00010001u8)), // schnellblink

            // 960..=1000 => self.light_binary(0b10101010u8.rotate_right(count / 20 as u32)), // schnellblink
            _ => self.light_binary(0b10101010u8.rotate_right(slowcount / 10 as u32)),
        }
    }
}

pub struct MagSwitch {
    pub pin: gpiob::PB3<Input<PullUp>>,
    debounce_timer: u8,
}
impl MagSwitch {
    const DEBOUNCE_TIME: u8 = 5;

    pub fn from(pin: gpiob::PB3<Input<PullUp>>) -> Self {
        MagSwitch {
            pin,
            debounce_timer: 0,
        }
    }

    pub fn press_debounced(&mut self) -> bool {
        if self.pin.is_high().unwrap() && self.debounce_timer == 0 && self.pin.check_interrupt() {
            self.debounce_timer = MagSwitch::DEBOUNCE_TIME;
            true
        } else {
            false
        }
    }

    fn update_debounce_timers(&mut self) {
        if self.debounce_timer > 0 {
            self.debounce_timer -= 1;
        }
    }
}

#[rtic::app(device = stm32f1xx_hal::stm32)]
const APP: () = {
    struct Resources {
        sound_timer: CountDownTimer<pac::TIM3>,
        midi_timer: CountDownTimer<pac::TIM1>,
        onboard_led: stm32f1xx_hal::gpio::gpioc::PC13<Output<PushPull>>,
        pwm: stm32f1xx_hal::pwm::Pwm<
            stm32::TIM4,
            Tim4NoRemap,
            pwm::C4,
            gpiob::PB9<Alternate<PushPull>>,
        >,
        #[init(None)]
        tones: Option<Vec<Tone, U8>>,
        midi_player: midi::Player,
        leds: LedRing,
        mag_switch: MagSwitch,
        #[init(0)]
        rot_count: u16,
        #[init(0)]
        tick: u32,
        #[init(u32::MAX / 2)]
        last_tick: u32,
        rot_periods: HistoryBuffer<Option<u32>, U4>,
        #[init(0)]
        rotations: u8,
    }

    #[init]
    fn init(_cx: init::Context) -> init::LateResources {
        // get Peripherals
        let p = pac::Peripherals::take().unwrap();
        let mut flash = p.FLASH.constrain();
        let mut rcc = p.RCC.constrain();
        let mut afio = p.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(72.mhz())
            .pclk1(32.mhz())
            .adcclk(14.mhz())
            .freeze(&mut flash.acr);

        let mut gpioa = p.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = p.GPIOB.split(&mut rcc.apb2);
        let mut gpioc = p.GPIOC.split(&mut rcc.apb2);

        let leds = LedRing::from([
            gpioa.pa0.into_push_pull_output(&mut gpioa.crl).downgrade(),
            gpioa.pa1.into_push_pull_output(&mut gpioa.crl).downgrade(),
            gpioa.pa2.into_push_pull_output(&mut gpioa.crl).downgrade(),
            gpioa.pa3.into_push_pull_output(&mut gpioa.crl).downgrade(),
            gpiob.pb1.into_push_pull_output(&mut gpiob.crl).downgrade(),
            gpiob.pb10.into_push_pull_output(&mut gpiob.crh).downgrade(),
            gpiob.pb11.into_push_pull_output(&mut gpiob.crh).downgrade(),
            gpiob.pb0.into_push_pull_output(&mut gpiob.crl).downgrade(),
        ]);

        let (_pa15, pb3, _pb4) = afio.mapr.disable_jtag(gpioa.pa15, gpiob.pb3, gpiob.pb4);
        let mut mag_pin = pb3.into_pull_up_input(&mut gpiob.crl);
        mag_pin.make_interrupt_source(&mut afio);
        mag_pin.trigger_on_edge(&p.EXTI, Edge::FALLING);
        mag_pin.enable_interrupt(&p.EXTI);
        let mag_switch = MagSwitch::from(mag_pin);

        let mut onboard_led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
        onboard_led.set_low().unwrap();

        let sound_pin = gpiob.pb9.into_alternate_push_pull(&mut gpiob.crh);
        let mut pwm = Timer::tim4(p.TIM4, &clocks, &mut rcc.apb1).pwm(
            sound_pin,
            &mut afio.mapr,
            (CARRIER_FREQ).hz(),
        );
        pwm.enable(pwm::Channel::C4);

        // Recalculate the pwm width
        let mut sound_timer =
            Timer::tim3(p.TIM3, &clocks, &mut rcc.apb1).start_count_down(SOUND_UPDATE_FREQ.hz());
        sound_timer.listen(Event::Update);

        let mut midi_timer =
            Timer::tim1(p.TIM1, &clocks, &mut rcc.apb2).start_count_down(MAIN_FREQ.hz());
        midi_timer.listen(Event::Update);

        let midi_player = midi::Player::new();

        init::LateResources {
            sound_timer,
            midi_timer,
            midi_player,
            onboard_led,
            mag_switch,
            rot_periods: heapless::HistoryBuffer::new_with(None),
            pwm,
            leds,
        }
    }

    #[idle]
    fn idle(_c: idle::Context) -> ! {
        loop {
            cortex_m::asm::wfi();
        }
    }

    /// Does frequent regular "Housekeeping" Tasks: Midi Player update, Button Debounce, and led
    /// lights
    #[task(binds = TIM1_UP, resources = [mag_switch, leds, midi_timer, tones, midi_player, rot_periods, rotations, tick, last_tick], priority = 2)]
    fn status_updates(mut c: status_updates::Context) {
        static mut PROGRESS: u32 = 0;
        static mut STATE: u8 = 0;
        let midi_player = c.resources.midi_player;

        let mut midi_tones = None;
        let tick = c.resources.tick;
        *tick = tick.wrapping_add(1);

        let periods = c.resources.rot_periods;

        let current_period = tick.wrapping_sub(*c.resources.last_tick);
        let valid_iter = periods.as_slice().iter().filter_map(|t| *t);
        let valid_iter_len = periods.as_slice().iter().filter_map(|t| *t).count();
        let avg_period = (valid_iter.sum::<u32>() + current_period) / (valid_iter_len as u32 + 1);
        match *STATE {
            0 | 1 => {
                if avg_period < 50 {
                    // let speed = 1000 / (18 * avg_period); // plate turns per second with avg_period in ms
                    // prog += speed * 0.01; // speed * time;
                    // prog += 10 / (18 * 10 * avg_period); // in fraction of a full turn
                    // prog += 240 / (18 * 10 * avg_period); // in led_segments
                    // prog += 2400 / (18 * 10 * avg_period); // in  1/10 led_segments - estimated value ~ 4
                    *PROGRESS += 1200 / (9 * avg_period + 1);
                    midi_player.play();
                    let updatetime = match avg_period {
                        0..=10 => 20 * (100 / MAIN_FREQ),
                        11..=20 => 15 * (100 / MAIN_FREQ),
                        21..=30 => 10 * (100 / MAIN_FREQ),
                        31..=40 => 5 * (100 / MAIN_FREQ),
                        41..=50 => 3 * (100 / MAIN_FREQ),
                        51..=100 => 2 * (100 / MAIN_FREQ),
                        _ => 1 * (100 / MAIN_FREQ),
                    };
                    match midi_player.update(updatetime) {
                        Action::Stopped => {
                            *STATE = *STATE + 1;
                            *PROGRESS = 0;
                            midi_player.play();
                        }
                        Action::None => {}
                    }
                    midi_tones = midi_player.get_tones();
                } else {
                    midi_player.pause();
                    c.resources.tones.lock(|t| *t = None);
                }
                c.resources.leds.light1(*PROGRESS);
            }
            2 | 3 => {
                midi_player.set_song(KuchenDrehSongs::HochSoll);
                c.resources.leds.light2(*PROGRESS);
                *PROGRESS += 10;
                match midi_player.update(10) {
                    Action::Stopped => {
                        midi_player.play();
                        *STATE = *STATE + 1;
                        *PROGRESS = 0;
                        if *STATE == 4 {
                            midi_player.set_song(KuchenDrehSongs::HappyBirthday);
                            c.resources.leds.light_binary(0);
                            *STATE = 0;
                        }
                    }
                    Action::None => {}
                }
                midi_tones = midi_player.get_tones();
            }
            _ => *STATE = 0,
        }

        c.resources.tones.lock(|t| *t = midi_tones);
        c.resources.mag_switch.update_debounce_timers();

        c.resources.midi_timer.clear_update_interrupt_flag();
    }

    #[task(binds = TIM3, resources = [sound_timer, pwm, tones], priority=3)]
    fn set_sound_output(c: set_sound_output::Context) {
        static mut SOUND_COUNTER: u16 = 0;
        *SOUND_COUNTER = SOUND_COUNTER.wrapping_add(1);

        if let Some(tones) = c.resources.tones {
            let duty = calc_amp(*SOUND_COUNTER, tones);
            c.resources.pwm.set_duty(pwm::Channel::C4, duty);
        } else {
            // PWM duty should be 50% if no tone is played. Otherwise there would be a cracking
            // sound when a tone stops. (tone oszilates around 50%)
            c.resources.pwm.set_duty(pwm::Channel::C4, ((72_000_000 / 2) / CARRIER_FREQ) as u16);
        }

        c.resources.sound_timer.clear_update_interrupt_flag();
    }

    #[task(binds = EXTI3, resources = [mag_switch, last_tick, rot_periods, rotations, tick], priority = 1)]
    fn exti3(mut c: exti3::Context) {
        let mut mag_switch = c.resources.mag_switch;
        if mag_switch.lock(|m| m.press_debounced()) {
            c.resources.rotations.lock(|r| *r = r.wrapping_add(1));
            let tick = c.resources.tick.lock(|t| *t);
            let mut period = c.resources.last_tick.lock(|last_tick| tick - *last_tick);
            if period > 100 {
                period = 100;
            }
            c.resources.rot_periods.lock(|p| p.write(Some(period)));
            c.resources.last_tick.lock(|last_tick| *last_tick = tick);
            mag_switch.lock(|m| m.pin.clear_interrupt_pending_bit());
        }
    }
};
