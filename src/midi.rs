#![allow(dead_code)]
use crate::tones::*;
use heapless::{
    consts::{U2, U4, U8},
    FnvIndexSet, Vec,
};
use stm32f1xx_hal::time::U32Ext;

/// The Midi Specification describes two different methods of how time can be notated. Ticks and
/// Seconds.
#[derive(Debug, Copy, Clone)]
enum DeltaTimes {
    Ticks(u16),
    Seconds((i8, u8)), // subdivisions of a second
}
impl DeltaTimes {
    fn from(division: &[u8; 2]) -> Self {
        if division[0] & 0x8 == 0x8 {
            let smpte = match (division[1] & 0x7) as i8 {
                -24 => -24,
                -25 => -25,
                -29 => -29,
                -30 => -30,
                _ => panic! {"Invalid Midi"},
            };
            DeltaTimes::Seconds((smpte, division[0]))
        } else {
            let ticks = u16::from_be_bytes(*division);
            DeltaTimes::Ticks(ticks)
        }
    }
}

// fn test_decode_vlq() {
// let inp: [u8; 2] = [0xc0, 0x00];
// assert_eq!(decode_vlq(&inp), (0x2000, 2));
// let inp: [u8; 2] = [0xff, 0x7f];
// assert_eq!(decode_vlq(&inp), (0x3FFF, 2));
// let inp: [u8; 3] = [0xff, 0xff, 0x7f];
// assert_eq!(decode_vlq(&inp), (0x1FFFFF, 3));
// let inp: [u8; 4] = [0xff, 0xff, 0x7f, 0xab];
// assert_eq!(decode_vlq(&inp), (0x1FFFFF, 3));
// let inp: [u8; 4] = [0x81, 0x80, 0x00, 0xab];
// assert_eq!(decode_vlq(&inp), (0x4000, 3));
// let inp: [u8; 4] = [0xFF, 0xFF, 0xFF, 0x7f];
// assert_eq!(decode_vlq(&inp), (0x0FFFFFFF, 4));
// let inp: [u8; 5] = [0xFF, 0xFF, 0xFF, 0x7f, 0x00];
// assert_eq!(decode_vlq(&inp), (0x0FFFFFFF, 4));
// let inp: [u8; 5] = [0xFF, 0xFF, 0xFF, 0x7f, 0x80];
// assert_eq!(decode_vlq(&inp), (0x0FFFFFFF, 4));
// }

/// Decode variable-length encoded numbers to `u32` values. The second returned parameter specifies
/// the number of bytes of the variable-legth encoded number.
fn decode_vlq(data: &[u8]) -> (u32, usize) {
    let mut decoded: u32 = 0;
    let mut len = 0;
    while data[len] & 0x80 == 0x80 {
        decoded += (0x7f & data[len]) as u32;
        decoded <<= 7;
        len += 1;
    }
    decoded += (0x7f & data[len]) as u32;

    (decoded, len + 1)
}

// Length of the MIDI Header fields
const TYPE_L: usize = 4;
const LENGTH_L: usize = 4;
const FORMAT_L: usize = 2;
const NR_TRACKS_L: usize = 2;
const DIVIS_L: usize = 2;

#[derive(Debug)]
struct Channel {
    keys: FnvIndexSet<u8, U4>,
}
impl Channel {
    fn new() -> Self {
        Channel {
            keys: FnvIndexSet::new(),
        }
    }
    fn start_key(&mut self, key: u8) {
        self.keys
            .insert(key)
            .expect("Too much notes simultaneously");
    }
    fn stop_key(&mut self, key: u8) {
        self.keys.remove(&key);
    }
}

enum MidiEvent {
    TempoChange(u32), // New tempo in us/beat
    TrackFinished,
    Nothing,
}

#[derive(Debug)]
struct Track {
    data: &'static [u8],
    // length: u32,
    channels: [Channel; 8],
    next_event_ticks: u32,
    status: u8,
    counter: usize,
    id: u16,
}
impl Track {
    fn from(data: &'static [u8], id: u16) -> Self {
        let mut counter = TYPE_L + LENGTH_L;
        let (delta_time, incr) = decode_vlq(array_ref![data, counter, 4]);
        counter += incr;

        Track {
            data,
            channels: [
                Channel::new(),
                Channel::new(),
                Channel::new(),
                Channel::new(),
                Channel::new(),
                Channel::new(),
                Channel::new(),
                Channel::new(),
                // Channel::new(),
                // Channel::new(),
                // Channel::new(),
                // Channel::new(),
            ],
            next_event_ticks: delta_time,
            status: 0x00,
            counter,
            id,
        }
    }

    fn reset(&mut self) {
        self.counter = TYPE_L + LENGTH_L;
        let (delta_time, incr) = decode_vlq(array_ref![self.data, self.counter, 4]);
        self.next_event_ticks = delta_time;
        self.counter += incr;
        self.status = 0x00;
        self.channels.iter_mut().for_each(|c| c.keys.clear());
    }

    /// steps the next `ticks` in the song and updates the channel's values.
    fn update(&mut self, ticks: u32) -> MidiEvent {
        let mut ret_val = MidiEvent::Nothing;
        while self.next_event_ticks < ticks {
            let mut len: usize = 0; // len is the length of the command and the data
            if self.data[self.counter] & 0x80 == 0x80 {
                // new Status Byte
                self.status = self.data[self.counter];
                self.counter += 1;
            }
            let mtrk_event = self.status;
            match mtrk_event {
                // Note off
                0x80..=0x8F => {
                    let channel = (mtrk_event & 0x0F) as usize;
                    let key = self.data[self.counter];
                    // let velocity = self.data[self.counter + i + 1];
                    self.channels[channel].stop_key(key);
                    len += 2;
                }
                0x90..=0x9F => {
                    // Note on
                    let channel = (mtrk_event & 0x0F) as usize;
                    let key = self.data[self.counter];
                    let velocity = self.data[self.counter + 1];
                    if velocity != 0 {
                        self.channels[channel].start_key(key);
                    } else {
                        self.channels[channel].stop_key(key);
                    }
                    len += 2;
                }
                0xB0..=0xBF => {
                    // Control channel
                    // let channel = (mtrk_event & 0x0F) as usize;
                    // let function = self.data[self.counter];
                    // let other = self.data[self.counter + 1];
                    // TODO: Implement Special Channel Mode Messages
                    len += 2;
                }
                0xC0..=0xCF => {
                    // Program(Instrument) change
                    // TODO: Implement
                    // let channel = (mtrk_event & 0x0F) as usize;
                    // let instrument = self.data[self.counter];
                    len += 1;
                }
                // 0xF0 => {} // System Exclusive + Variable Length Quantity
                // 0xF7 => {} // End of exclusive
                0xFF => {
                    // Meta Event
                    match self.data[self.counter] {
                        0x01..=0x07 => {}                        // text messages
                        0x2F => return MidiEvent::TrackFinished, // End of Track
                        0x51 => {
                            let c = self.counter;
                            let us_quarter_note = u32::from_be_bytes([
                                0x0,
                                self.data[c + 2],
                                self.data[c + 3],
                                self.data[c + 4],
                            ]);
                            ret_val = MidiEvent::TempoChange(us_quarter_note);
                        } // Set Tempo
                        0x54 => {}                               // SMPTE Offset TODO: Implement
                        0x58 => {}                               // Time Signature
                        0x59 => {}                               // Key Signature
                        _ => {
                            // hprintln! {"Unimplemented Meta Event {:2X} {:2X} at pos {:2X}",
                            // self.data[self.counter], self.data[self.counter + 1], self.counter};
                        }
                    };
                    let (l, inc) = decode_vlq(array_ref![self.data, self.counter + 1, 4]);
                    len += l as usize + inc + 1;
                }
                _ => {
                    panic! {"Unimplemented MIDI Code {:2x} at pos {:2x}", self.data[self.counter], self.counter}
                }
            }
            self.counter += len;

            let (delta_time, incr) = decode_vlq(array_ref![self.data, self.counter, 4]);
            self.next_event_ticks += delta_time;
            self.counter += incr;
        }
        ret_val
    }
}

#[derive(Clone, Copy)]
pub enum KuchenDrehSongs {
    HappyBirthday = 0,
    HochSoll = 1,
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum PlayerState {
    Play,
    Pause,
    Stopped,
}

pub struct Player {
    state: PlayerState,
    pub current_song: KuchenDrehSongs,
    avail_songs: [Song; 2],
}
impl Player {
    pub fn new() -> Self {
        Player {
            state: PlayerState::Stopped,
            current_song: KuchenDrehSongs::HappyBirthday,
            avail_songs: [
                Song::from(include_bytes!("../assets/midi/happy_birthday.mid")),
                Song::from(include_bytes!("../assets/midi/hochsol.mid")),
            ],
        }
    }

    fn cur_song(&mut self) -> &mut Song {
        &mut self.avail_songs[self.current_song as usize]
    }

    pub fn status(&mut self) -> PlayerState {
        self.state
    }

    pub fn stop(&mut self) {
        self.cur_song().reset();
        self.state = PlayerState::Stopped;
    }

    pub fn play(&mut self) {
        self.state = PlayerState::Play;
    }

    pub fn pause(&mut self) {
        self.state = PlayerState::Pause;
    }

    pub fn play_pause(&mut self) {
        match self.state {
            PlayerState::Play => self.state = PlayerState::Pause,
            PlayerState::Pause | PlayerState::Stopped => self.state = PlayerState::Play,
        };
    }

    /// Tells the caller which LEDs to light
    pub fn get_leds(&mut self) -> Option<[bool; 8]> {
        if let PlayerState::Play = self.state {
            Some(self.cur_song().get_leds())
        } else {
            None
        }
    }

    pub fn get_tones(&mut self) -> Option<Vec<Tone, U8>> {
        if let PlayerState::Play = self.state {
            Some(self.cur_song().get_tones())
        } else {
            None
        }
    }

    /// This function updates all Tracks and channels. `time_passed` is the time since the last
    /// call in ms
    pub fn update(&mut self, time_passed: u32) -> Action {
        let action = match &self.state {
            PlayerState::Play => self.cur_song().update(time_passed),
            PlayerState::Pause | PlayerState::Stopped => Action::None,
        };
        match action {
            Action::Stopped => self.stop(),
            _ => {}
        }
        action
    }

    pub fn set_song(&mut self, new_song: KuchenDrehSongs) {
        self.current_song = new_song;
    }
}

#[derive(PartialEq)]
pub enum Action {
    Stopped,
    None,
}
// #[derive(Debug)]
struct Song {
    ticks: u32,
    division: DeltaTimes,
    tracks: Vec<Track, U2>,
    tempo: u32, // in ms/beat
}
impl Song {
    fn from(data: &'static [u8]) -> Self {
        // Sample header:
        // |----MThd----|  |---length---|  |fmt-|  |#Trks| |Divs|
        // 4d  54  68  64  00  00  00  06  00  01  00  04  04  00

        // |----MTrk----|  |---length---|
        // 4d  54  72  6b  00  00  1e  f5
        //
        // Meta event (ff) after 0 ticks of type 54 with 5 bytes (0,0,0,0,0)
        // 00   ff   54   05   00   00   00   00   00
        // (Here: SMPTE Event at which track is supposed to start)

        if array_ref![data, 0, 4] != b"MThd" {
            panic! {"Invalid Midi"};
        }

        // this is actually always 6...
        let header_length: u32 = u32::from_be_bytes(*array_ref![data, TYPE_L, LENGTH_L]);
        let nr_tracks =
            u16::from_be_bytes(*array_ref![data, TYPE_L + LENGTH_L + FORMAT_L, NR_TRACKS_L]);
        // if header_format != 0 || nr_tracks != 1 {
        // panic! {"Only MIDI v1 is supported"};
        // }
        let division = DeltaTimes::from(array_ref![
            data,
            TYPE_L + LENGTH_L + FORMAT_L + NR_TRACKS_L,
            DIVIS_L
        ]);

        // ===================== Header done =====================

        let mut track_begin = header_length as usize + TYPE_L + LENGTH_L;
        let mut tracks = Vec::new();
        for i in 0..nr_tracks {
            if array_ref![data, track_begin, 4] != b"MTrk" {
                panic! {"Invalid Midi Header"};
            }
            let track_length: u32 =
                u32::from_be_bytes(*array_ref![data, track_begin + TYPE_L, LENGTH_L]);
            let track_end = track_begin + TYPE_L + LENGTH_L + track_length as usize - 1;
            let data = &data[track_begin..=track_end];

            tracks
                .push(Track::from(&data, i))
                .expect("More MIDI Tracks than system can handle");
            track_begin = track_end + 1;
        }

        Song {
            ticks: 0,
            division,
            tracks,
            tempo: 100,
        }
    }

    fn reset(&mut self) {
        self.ticks = 0;
        self.tracks.iter_mut().for_each(|t| t.reset());
    }

    /// This function updates all Tracks and channels. `time_passed` is the time since the last
    /// call in ms
    fn update(&mut self, time_passed: u32) -> Action {
        if let DeltaTimes::Ticks(ticks_per_quarter) = self.division {
            self.ticks += time_passed * ticks_per_quarter as u32 / self.tempo;
        } else {
            panic! {"SMPTE not implemented"}
        }

        for i in 0..self.tracks.len() {
            match self.tracks[i].update(self.ticks) {
                MidiEvent::Nothing => {}
                MidiEvent::TrackFinished => return Action::Stopped,
                MidiEvent::TempoChange(new_tempo) => self.tempo = new_tempo / 1000, // reduce accuracy here to avoid u32 overflow. We calculate in ms
            }
        }
        Action::None
    }

    /// Tells the caller which tones are currently played
    fn get_tones(&mut self) -> Vec<Tone, U8> {
        self.tracks
            .iter()
            .flat_map(|track| track.channels.iter().flat_map(|chan| chan.keys.iter()))
            .map(|key| Tone::sine((MIDIFREQ[*key as usize] as u32).hz()))
            .collect()
    }

    /// Tells the caller which LEDs to light
    fn get_leds(&mut self) -> [bool; 8] {
        let mut leds: [bool; 8] = [false; 8];
        self.tracks
            .iter()
            .flat_map(|track| track.channels.iter().flat_map(|chan| chan.keys.iter()))
            .for_each(|key| leds[(key % 8) as usize] = true);
        leds
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Scale {
    Major,
    Minor,
}
impl Scale {
    pub fn as_midi_steps(&self) -> [u8; 8] {
        match self {
            Scale::Major => [0, 2, 4, 5, 7, 9, 11, 12],
            Scale::Minor => [0, 2, 3, 5, 7, 8, 10, 12],
        }
    }
    pub fn as_str(&self) -> &'static str {
        match self {
            Scale::Major => "Dur",
            Scale::Minor => "Moll",
        }
    }

    pub fn next(&self) -> Scale {
        match self {
            Scale::Major => Scale::Minor,
            Scale::Minor => Scale::Major,
        }
    }

    pub fn prev(&self) -> Scale {
        match self {
            Scale::Major => Scale::Minor,
            Scale::Minor => Scale::Major,
        }
    }
}

// TODO: Put this in Midi or Tone
#[derive(Debug, Copy, Clone)]
pub enum Key {
    C = 0,
    Cis = 1,
    D = 2,
    Dis = 3,
    E = 4,
    F = 5,
    Fis = 6,
    G = 7,
    Gis = 8,
    A = 9,
    Ais = 10,
    H = 11,
}
impl Key {
    pub fn as_str(&self) -> &'static str {
        match self {
            Key::C => "C",
            Key::Cis => "C#/Db",
            Key::D => "D",
            Key::Dis => "D#/Eb",
            Key::E => "E",
            Key::F => "F",
            Key::Fis => "F#/Gb",
            Key::G => "G",
            Key::Gis => "G#/Ab",
            Key::A => "A",
            Key::Ais => "b",
            Key::H => "H",
        }
    }

    pub fn as_midi_key(&self) -> u8 {
        *self as u8
    }

    pub fn from(i: u8) -> Self {
        match i % 12 {
            0 => Key::C,
            1 => Key::Cis,
            2 => Key::D,
            3 => Key::Dis,
            4 => Key::E,
            5 => Key::F,
            6 => Key::Fis,
            7 => Key::G,
            8 => Key::Gis,
            9 => Key::A,
            10 => Key::Ais,
            11 => Key::H,
            _ => unreachable!(),
        }
    }

    pub fn next(&self) -> Key {
        if (*self as u8) == 11 {
            Key::C
        } else {
            Key::from(*self as u8 + 1)
        }
    }

    pub fn prev(&self) -> Key {
        if (*self as u8) == 0 {
            Key::H
        } else {
            Key::from(*self as u8 - 1)
        }
    }
}

pub fn pad_to_midi(pads: &[bool; 8], key: Key, scale: Scale, octave: u8) -> Vec<u8, U8> {
    let mut scale_tones = scale.as_midi_steps();
    scale_tones
        .iter_mut()
        .for_each(|t| *t += key.as_midi_key() + octave * 12);
    pads.iter()
        .enumerate()
        .filter(|(_i, p)| **p)
        .map(|(i, _p)| scale_tones[i])
        .collect()
}

pub fn midi_to_tones(midis: &Vec<u8, U8>, waveform: Waveform) -> Vec<Tone, U8> {
    midis
        .iter()
        .map(|key| Tone {
            freq: (MIDIFREQ[*key as usize] as u32).hz(),
            waveform: waveform,
        })
        .collect()
}

const MIDIFREQ: [u16; 128] = [
    1,    // −20  C -1
    1,    // −19
    1,    // −18
    1,    // −17
    1,    // −16
    1,    // −15
    1,    // −14
    1,    // −13
    1,    // −12
    1,    // −11
    1,    // −10  A# MIDI KEY 10
    1,    // −9
    16,   // −8   C2
    17,   // −7   Cis2/Des2
    18,   // −6   D2
    19,   // −5   Dis2/Es2
    20,   // −4   E2
    21,   // −3   F2
    23,   // −2   Fis2/Ges2
    24,   // −1   G2
    26,   // 0    Gis2/As2 MIDI KEY 20
    28,   // 1    A2
    29,   // 2    Ais2/B2
    31,   // 3    H2
    33,   // 4    C1
    35,   // 5    Cis1/Des1
    37,   // 6    D1
    39,   // 7    Dis1/Es1
    41,   // 8    E1
    44,   // 9    F1
    46,   // 10   Fis1/Ges1
    49,   // 11   G1
    52,   // 12   Gis1/As1
    55,   // 13   A1
    58,   // 14   Ais1/B1
    62,   // 15   H1
    65,   // 16   C
    69,   // 17   Cis/Des
    73,   // 18   D
    78,   // 19   Dis/Es
    82,   // 20   E
    87,   // 21   F
    92,   // 22   Fis/Ges
    98,   // 23   G
    104,  // 24   Gis/As
    110,  // 25   A
    117,  // 26   Ais/B
    123,  // 27   H
    131,  // 28   c
    139,  // 29   cis/des
    147,  // 30   d
    156,  // 31   dis/es
    165,  // 32   e
    175,  // 33   f
    185,  // 34   fis/ges
    196,  // 35   g
    208,  // 36   gis/as
    220,  // 37   a
    233,  // 38   ais/b
    247,  // 39   h
    262,  // 40   c1 MIDI KEY 60
    277,  // 41   cis1/des1
    294,  // 42   d1
    311,  // 43   dis1/es1
    330,  // 44   e1
    349,  // 45   f1
    370,  // 46   fis1/ges1
    392,  // 47   g1
    415,  // 48   gis1/as1
    440,  // 49   a1
    466,  // 50   ais1/b1
    494,  // 51   h1
    523,  // 52   c2
    554,  // 53   cis2/des2
    587,  // 54   d2
    622,  // 55   dis2/es2
    659,  // 56   e2
    698,  // 57   f2
    740,  // 58   fis2/ges2
    784,  // 59   g2
    831,  // 60   gis2/as2
    880,  // 61   a2
    932,  // 62   ais2/b2
    989,  // 63   h2
    1047, // 64   c3
    1109, // 65   cis3/des3
    1175, // 66   d3
    1245, // 67   dis3/es3
    1319, // 68   e3
    1397, // 69   f3
    1480, // 70   fis3/ges3
    1568, // 71   g3
    1661, // 72   gis3/as3
    1760, // 73   a3
    1865, // 74   ais3/b3
    1976, // 75   h3
    2093, // 76   c4
    2217, // 77   cis4/des4
    2349, // 78   d4
    2489, // 79   dis4/es4
    2637, // 80   e4 MIDI KEY 100
    2794, // 81   f4
    2960, // 82   fis4/ges4
    3136, // 83   g4
    3322, // 84   gis4/as4
    3520, // 85   a4
    3729, // 86   ais4/b4
    3951, // 87   h4
    4186, // 88   c5
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
    4200, //
];
