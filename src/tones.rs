use crate::{CARRIER_FREQ, SOUND_UPDATE_FREQ};
use heapless::{consts::U8, Vec};
use stm32f1xx_hal::time::Hertz;

const TABLE_LENGTH: usize = 128;
const AMPLITUDE: u16 = 128;

#[derive(Debug, Copy, Clone)]
pub enum Waveform {
    Sine,
    Square,
    Saw,
}
impl Waveform {
    pub fn as_str(&self) -> &'static str {
        match self {
            Waveform::Sine => "Sine",
            Waveform::Square => "Square",
            Waveform::Saw => "Saw",
        }
    }

    pub fn next(&self) -> Waveform {
        match self {
            Waveform::Sine => Waveform::Square,
            Waveform::Square => Waveform::Saw,
            Waveform::Saw => Waveform::Sine,
        }
    }
    pub fn prev(&self) -> Waveform {
        match self {
            Waveform::Sine => Waveform::Saw,
            Waveform::Square => Waveform::Sine,
            Waveform::Saw => Waveform::Square,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Tone {
    pub freq: Hertz,
    pub waveform: Waveform,
    // Idea: Add amplitude
}
impl Tone {
    pub fn sine(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Sine,
        }
    }
    pub fn square(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Square,
        }
    }
    pub fn saw(f: Hertz) -> Self {
        Tone {
            freq: f,
            waveform: Waveform::Saw,
        }
    }
}

/// Outputs the current amplitude of the resulting tone from tones.
/// This method must be called at a frequency of 8196 Hz and outputs values in the range [0:3600]
pub fn calc_amp(timestep: u16, tones: &Vec<Tone, U8>) -> u16 {
    // static mut
    let mut amp = 0;
    let mut div = 0;
    // induced by the frequency this function is called with. It equals
    // tim3.freq() / steps
    let frequ_corr = SOUND_UPDATE_FREQ / 256;

    // amp_corr = (Max duty / max value in sine table)
    // Our Sine Table reaches to 128. Max_duty is 2048
    //
    // 35000 => 2048
    // 16000 => 4080
    // 70000 => 1024
    let amp_corr = (72_000_000 / CARRIER_FREQ) as u16 / 128;
    for tone in tones.iter() {
        let progress_in_wave =
            (((timestep as u32 * tone.freq.0) / frequ_corr) % TABLE_LENGTH as u32) as usize;
        amp += match tone.waveform {
            Waveform::Sine => SINE_128[progress_in_wave],
            Waveform::Square => {
                if progress_in_wave > (TABLE_LENGTH / 2) {
                    AMPLITUDE
                } else {
                    0
                }
            }
            Waveform::Saw => progress_in_wave as u16,
        };
        div += 1;
    }
    if div != 0 {
        // TODO: durch sqrt(2) teilen bei zwei tönen
        amp * amp_corr / div
    } else {
        0
    }
}

const SINE_128: [u16; TABLE_LENGTH] = [
    64, 67, 70, 73, 76, 79, 82, 85, 88, 91, 94, 96, 99, 102, 104, 106, 109, 111, 113, 115, 116,
    118, 120, 121, 122, 123, 124, 125, 126, 126, 127, 127, 127, 127, 127, 126, 126, 125, 124, 123,
    122, 121, 120, 118, 116, 115, 113, 111, 109, 106, 104, 102, 99, 96, 94, 91, 88, 85, 82, 79, 76,
    73, 70, 67, 64, 61, 58, 55, 52, 49, 46, 43, 40, 37, 34, 32, 29, 26, 24, 22, 19, 17, 15, 13, 12,
    10, 8, 7, 6, 5, 4, 3, 2, 2, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 10, 12, 13, 15, 17, 19, 22,
    24, 26, 29, 32, 34, 37, 40, 43, 46, 49, 52, 55, 58, 61,
];
